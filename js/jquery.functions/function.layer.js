(function($) {
    /* --- LAYER MODULE --- */

    $.fn.layer = function(options, callback) {
        var $coll = $(this);
        var settings = $.extend({
            duration: (options && options.duration) ? options.duration : 400
        }, options);

        $.each($coll, function(i, element) {
            var $btn = $(this);
            var $overlay = $('.js-layer__overlay');

            $('#'+$btn.attr('aria-controls')).hide();
            $btn.addClass('js-layer--off').attr('aria-expanded', false);

            $btn.on('click', function(e) {
                var $btn = $(this);
                var target = '#' + $btn.attr('aria-controls');
                var name = $btn.attr('name');
                if (element.tagName == 'A') e.preventDefault();

                // toggle target panel & toggle class
                if ($btn.hasClass('js-layer--on')) {
                    $(target).not(':animated').slideUp(settings.duration, function() {
                        if (typeof callback != 'undefined') {
                            callback();
                        }

                        $overlay.removeClass('on');
                        $btn.removeClass('js-layer--on').addClass('js-layer--off').attr('aria-expanded', false);

                        if ($btn.attr('data-layer-modifier')) $(target).removeClass('c-layer--'+$btn.data('layer-modifier'));
                        if ($btn.hasClass('js-layer--picto')) {
                            $('body').find('.js-layer__picto').remove();
                        }
                    });
                } else {
                    if ($btn.attr('data-layer-modifier')) $(target).addClass('c-layer--'+$btn.data('layer-modifier'));
                    $(target).show()
                    var tWidth = $(target).outerWidth();
                    var bWidth = $btn.outerWidth();
                    var bHeight = $btn.outerHeight();
                    var bLeft = $btn.offset().left;
                    var bTop = $btn.offset().top;
                    var containerLeft = $('.c-header > .container').offset().left + 15;
                    var resultLeft = bLeft+bWidth/2-tWidth/2;
                    var resultTop = bTop+bHeight+10;

                    if (resultLeft < containerLeft) resultLeft = containerLeft;

                    $(target).css({
                        left: resultLeft,
                        top: resultTop
                    });

                    if ($('.js-layer--on').length) {
                        $('.js-layer--on').trigger('click');
                    }

                    $(target).hide().not(':animated').slideDown(settings.duration, function() {
                        if (typeof callback != 'undefined') {
                            callback();
                        }

                        if ($btn.data('layer-modifier') != 'addcompare') $overlay.addClass('on');
                        $btn.removeClass('js-layer--off').addClass('js-layer--on').attr('aria-expanded', true);

                        if ($btn.hasClass('js-layer--picto')) {
                            $('body').append('<img class="c-layer__picto js-layer__picto" src="img/picto-popover.svg" alt="">');
                            $('body').find('.js-layer__picto').css({
                                left: bLeft+bWidth/2-10,
                                top: resultTop
                            });
                        }
                    }).data('name', name);
                }
            });

            $overlay.click(function() {
                var $openedLayer = $('.js-layer.js-layer--on');

                if (!$('#addcompare').hasClass('js-layer--on')) $openedLayer.trigger('click');
            });
        });
        return $(this);
    };
})(jQuery);