(function($) {
    /* --- UPDATEINFO MODULE --- */

    $.fn.updateinfo = function() {
        var $wrap = $(this);

        $.each($wrap, function(i, element) {
            var $elm = $(element);
            var info = $elm.data('info');
            
            if (info == 'departure') {
                var $calendar = $elm.find('.js-updateinfo__calendar');
                var options = { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };

                var $datepicker = $calendar.datepicker({
                    onSelect: function(formattedDate, date, inst) {
                        var $layer = $elm;
                        var name = $layer.data('name');
                        var $srcBtn = $('[name="'+name+'"]');
                        var $mainBtn = $('[name="btn-layerCalendar"]');

                        if (date) {
                            $calendar.data('date', date);

                            var date = date.toLocaleDateString("fr-FR", options);

                            date = date.split(' ');
                            date = date[1]+' '+date[2]+' '+date[3];
                            $calendar.trigger('clickdate');
                            $('.js-updateinfo__target[data-info="'+info+'"]').html(date);
                            $mainBtn.addClass('on').find('.c-btn__picto--arrow').hide();
                            $mainBtn.find('.c-btn__picto--close').show();
                            if (name != 'addcompare') $srcBtn.trigger('click');
                        } else {
                            $datepicker.selectDate($calendar.data('date'));
                            if (name != 'addcompare') $srcBtn.trigger('click');
                        }
                    }
                }).data('datepicker');
            }
            
            if (info == 'departure-a' || info == 'departure-b' || info == 'departure-c') {
                var $calendar = $elm.find('.js-updateinfo__calendar');
                var options = { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };
                var selectedDate = new Date('2019-02-16T00:00:00');

                var $datepicker = $calendar.datepicker({
                    onSelect: function(formattedDate, date, inst) {
                        var $layer = $elm;
                        var name = $layer.data('name');
                        var $srcBtn = $('[name="'+name+'"]');
                        var $mainBtn = $('[name="btn-layerCalendar"]');

                        if (date) {
                            $calendar.data('date', date);

                            var date = date.toLocaleDateString("fr-FR", options);

                            date = date.split(' ');
                            date = 'Départ le '+date[1]+' '+date[2]+' '+date[3];
                            $('.js-updateinfo__target[data-info="'+info+'"]').html(date);
                            $srcBtn.parents('.js-compare__layer').trigger('change');
                            $srcBtn.trigger('click');
                        } else {
                            $('.js-updateinfo__target[data-info="'+info+'"]').html('Départ');
                        }
                    }
                }).data('datepicker');
                $datepicker.selectDate(selectedDate);
            }

            if (info == 'travelers' || info == 'travelers-a' || info == 'travelers-b' || info == 'travelers-c') {
                var travelers = [0, 0];
                var $btns = $elm.find('.js-updateinfo__less, .js-updateinfo__more');

                $btns.click(function() {
                    var $tBtn = $(this);
                    var $layer = $tBtn.parents('.c-layer');
                    var $children = $layer.find('.js-children');
                    var $childrenItems = $children.find('.c-children__item');
                    var name = $elm.data('name');
                    var $srcBtn = $('[name="'+name+'"]');

                    if ($tBtn.hasClass('js-updateinfo__more')) {
                        if ($tBtn.parents('.js-updateinfo__adults').length) {
                            if (travelers[0] < 10) {
                                travelers[0]++;
                                $tBtn.siblings('.js-updateinfo__value').html(travelers[0]).siblings('.js-updateinfo__less').removeAttr('disabled');
                                if (travelers[0] == 10) $tBtn.attr('disabled', '');
                            }
                        }

                        if ($tBtn.parents('.js-updateinfo__children').length) {
                            if (travelers[1] < 2) {
                                travelers[1]++;
                                $tBtn.siblings('.js-updateinfo__value').html(travelers[1]).siblings('.js-updateinfo__less').removeAttr('disabled');
                                if (travelers[1] == 2) $tBtn.attr('disabled', '');
                            }
                        }
                    } else if ($tBtn.hasClass('js-updateinfo__less')) {
                        if ($tBtn.parents('.js-updateinfo__adults').length) {
                            if (travelers[0] > 1) {
                                travelers[0]--;
                                $tBtn.siblings('.js-updateinfo__value').html(travelers[0]).siblings('.js-updateinfo__more').removeAttr('disabled');
                                if (travelers[0] == 1) $tBtn.attr('disabled', '');
                            }
                        }

                        if ($tBtn.parents('.js-updateinfo__children').length) {
                            if (travelers[1] > 0) {
                                travelers[1]--;
                                $tBtn.siblings('.js-updateinfo__value').html(travelers[1]).siblings('.js-updateinfo__more').removeAttr('disabled');
                                if (travelers[1] == 0) $tBtn.attr('disabled', '');
                            }
                        }
                    }

                    if (travelers[1] > 0) {
                        $childrenItems.eq(0).addClass('on');

                        if (!$children.hasClass('on')) {
                            $children.not(':animated').slideDown(400, function() {
                                $children.addClass('on');
                            });
                        }

                        if (travelers[1] == 1) {
                            $childrenItems.eq(1).removeClass('on');
                        } else if (travelers[1] == 2) {
                            $childrenItems.eq(1).addClass('on');
                        }
                    } else if (travelers[1] == 0 && $children.hasClass('on')) {
                        $children.not(':animated').slideUp(400, function() {
                            $children.removeClass('on');
                            $childrenItems.find('input').val('');
                        });
                    }

                    if (info == 'travelers') {
                        $srcBtn.addClass('on').find('.c-btn__picto--arrow').hide();
                        $srcBtn.find('.c-btn__picto--close').show();
                    }

                    $('.js-updateinfo__target[data-info="'+info+'"]').html(travelers[0]+'&nbsp;adulte(s) - '+travelers[1]+'&nbsp;enfant(s)');
                    $srcBtn.parents('.js-compare__layer').trigger('change');
                });
            }

            if (info == 'nofixeddate') {
                var $layer = $elm.parents('.c-layer');
                var $calendar = $layer.find('.js-calendar');
                var $length = $layer.find('.js-updateinfo__length');

                $elm.on('change', function() {
                    var name = $layer.data('name');
                    var $srcBtn = $('[name="'+name+'"]');

                    if ($elm.is(':checked')) {
                        var $datepicker = $calendar.datepicker().data('datepicker');

                        $layer.addClass('checkbox-on');
                        $datepicker.clear();
                        $srcBtn.find('.js-updateinfo__target').html('Départ');
                    } else {
                        $layer.removeClass('checkbox-on');
                    }
                });
            }

            if (info == 'transport' || info == 'transport-a' || info == 'transport-b' || info == 'transport-c') {
                var $select = $elm.find('.js-updateinfo__transport');

                $select.on('change', function() {
                    var name = $elm.data('name');
                    var $srcBtn = $('[name="'+name+'"]');
                    var text = $select.find('option:selected').text();

                    if ($select.val() == 0) {
                        $srcBtn.find('.c-btn__picto--no-transport').addClass('on');
                        $srcBtn.find('.c-btn__picto--transport').removeClass('on');
                    } else {
                        $srcBtn.find('.c-btn__picto--transport').addClass('on');
                        $srcBtn.find('.c-btn__picto--no-transport').removeClass('on');
                        text = 'De ' + text;
                    }

                    $('.js-updateinfo__target[data-info="'+info+'"]').html(text);
                    $srcBtn.trigger('click');

                    if (info == 'transport-a' || info == 'transport-b' || info == 'transport-c') {
                        var name = $elm.data('name');
                        var $srcBtn = $('[name="'+name+'"]');
                        
                        $srcBtn.parents('.js-compare__layer').trigger('change');
                    }
                });

                if (info == 'transport-a' || info == 'transport-b' || info == 'transport-c') {
                    var $srcBtn;

                    if (info == 'transport-a') $srcBtn = $('.js-layer[name="btn-layerTransportA"]');
                    if (info == 'transport-b') $srcBtn = $('.js-layer[name="btn-layerTransportB"]');
                    if (info == 'transport-c') $srcBtn = $('.js-layer[name="btn-layerTransportC"]');

                    $select.find('option').eq(1).attr('selected', '');
                    $srcBtn.find('.c-btn__text').html('De Paris');
                    $srcBtn.find('.c-btn__picto--transport').addClass('on');
                    $srcBtn.find('.c-btn__picto--no-transport').removeClass('on');
                }
            }

            if (info == 'close') {
                $elm.click(function() {
                    var $layer = $elm.parents('.c-layer');
                    var name = $layer.data('name');
                    var $srcBtn = $('[name="'+name+'"]');

                    $srcBtn.trigger('click');
                });
            }

            if (info == 'price-a' || info == 'price-b' || info == 'price-c') {
                $elm.on('change', function() {
                    var value = $elm.html();
                    var $target = $('.js-updateinfo__target[data-info="'+info+'"]');

                    $target.html(value).trigger('change');
                });
            }
        });
        return $(this);
    };
})(jQuery);