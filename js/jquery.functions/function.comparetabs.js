(function($) {
    /* --- COMPARETABS MODULE --- */

    $.fn.comparetabs = function() {
        var $wrap = $(this);

        $.each($wrap, function(i, element) {
            var $block = $(element);
            var $compareBlock = $block.siblings('.js-compare');
            var $items = $block.find('.js-comparetabs__item');

            $items.each(function() {
                var $item = $(this);
                var $btn = $item.find('.js-comparetabs__btn');

                $btn.click(function() {
                    var resort = $btn.data('resort');
                    var $layer = $compareBlock.find('.js-compare__layer[data-resort="'+resort+'"]');
                    var $details = $compareBlock.find('.js-compare__details[data-resort="'+resort+'"]');

                    $items.find('.js-comparetabs__btn.on').removeClass('on');
                    $compareBlock.find('.js-compare__layer.on').removeClass('on');
                    $compareBlock.find('.js-compare__details.on').removeClass('on');

                    $btn.addClass('on');
                    $layer.addClass('on');
                    $details.addClass('on');
                });
            });
            
            $items.eq(0).find('.js-comparetabs__btn').trigger('click');
        });
        return $(this);
    };
})(jQuery);