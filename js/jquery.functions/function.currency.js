(function ($) {
    /* --- CURRENCY --- */

    $.fn.currency = function(option) {
        var $obj = $(this);
        var valArr = [];

        $.each($obj, function(i, element) {
            var $elm = $(element);
            var options = {
                decimalPlaces: 0,
                digitGroupSeparator: ' ',
                decimalCharacterAlternative: ' '
            };
            var autoNum = new AutoNumeric($elm[0], options).french();

            $elm.html($elm.html().replace('.', ' '));

            $elm.on('change', function() {
                var val = parseInt($elm.html().replace(' ',''));
                
                $elm.html(val);
                autoNum.remove();
                autoNum = new AutoNumeric($elm[0], options).french();
                $elm.html($elm.html().replace('.', ' '));
            });
        });
        return $(this);
    };
})(jQuery);