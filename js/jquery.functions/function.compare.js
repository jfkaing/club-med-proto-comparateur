(function($) {
    /* --- COMPARE MODULE --- */

    $.fn.compare = function() {
        var $wrap = $(this);

        $.each($wrap, function(i, element) {
            var $block = $(element);
            var $items = $block.find('.js-compare__layer');
            var infos = [
                {
                    "name": "Ile Maurice, La Pointe aux Canonniers",
                    "title": "compare-resort-a-name.jpg",
                    "img": "compare-resort-a.jpg",
                    "flight": "compare-flight-ile-maurice.jpg",
                    "rank": "4",
                    "tabs": [
                        {
                            "name": "Supérieure",
                            "oldprice": "0",
                            "newprice": "4890",
                            "img": "resort-a-superieure.jpg"
                        }, {
                            "name": "Deluxe",
                            "oldprice": "0",
                            "newprice": "5730",
                            "img": "resort-a-deluxe.jpg"
                        }, {
                            "name": "Suite",
                            "oldprice": "0",
                            "newprice": "8040",
                            "img": "resort-a-suite.jpg"
                        }
                    ]
                }, {
                    "name": "Ile Maurice, La Plantation d’Albion Club Med",
                    "title": "compare-resort-b-name.jpg",
                    "img": "compare-resort-b.jpg",
                    "flight": "compare-flight-ile-maurice.jpg",
                    "rank": "5",
                    "tabs": [
                        {
                            "name": "Supérieure",
                            "oldprice": "4910",
                            "newprice": "4425",
                            "img": "resort-b-superieure.jpg"
                        }, {
                            "name": "Deluxe",
                            "oldprice": "6113",
                            "newprice": "5506",
                            "img": "resort-b-deluxe.jpg"
                        }, {
                            "name": "Suite",
                            "oldprice": "7335",
                            "newprice": "6607",
                            "img": "resort-b-suite.jpg"
                        }
                    ]
                }, {
                    "name": "Ile Maurice, Les Villas d’Albion",
                    "title": "compare-resort-c-name.jpg",
                    "img": "compare-resort-c.jpg",
                    "flight": "compare-flight-ile-maurice.jpg",
                    "rank": "5",
                    "tabs": [
                        {
                            "name": "Villa 2 chambres",
                            "oldprice": "8790",
                            "newprice": "7915",
                            "img": "resort-c-villa.jpg"
                        }
                    ]
                }, {
                    "name": "Guadeloupe - Antilles Françaises, La Caravelle",
                    "title": "compare-resort-d-name.jpg",
                    "img": "compare-resort-d.jpg",
                    "flight": "compare-flight-guadeloupe.jpg",
                    "rank": "4",
                    "tabs": [
                        {
                            "name": "Supérieure",
                            "oldprice": "0",
                            "newprice": "4120",
                            "img": "resort-d-superieure.jpg"
                        }
                    ]
                }, {
                    "name": "Guadeloupe - Antilles Françaises, Des Grenadines à St-Barth",
                    "title": "compare-resort-d-name.jpg",
                    "img": "compare-resort-d.jpg",
                    "flight": "compare-flight-guadeloupe.jpg",
                    "rank": "4",
                    "tabs": [
                        {
                            "name": "Supérieure",
                            "oldprice": "0",
                            "newprice": "4120",
                            "img": "resort-d-superieure.jpg"
                        }
                    ]
                }, {
                    "name": "Guadeloupe - Antilles Françaises, Îles vierges et Guadeloupe",
                    "title": "compare-resort-d-name.jpg",
                    "img": "compare-resort-d.jpg",
                    "flight": "compare-flight-guadeloupe.jpg",
                    "rank": "4",
                    "tabs": [
                        {
                            "name": "Supérieure",
                            "oldprice": "0",
                            "newprice": "4120",
                            "img": "resort-d-superieure.jpg"
                        }
                    ]
                }
            ];

            $('.js-updateinfo__more').each(function() {
                $(this).trigger('click').trigger('click');
            });
            
            $items.each(function(index, e) {
                var $item = $(e);
                var $destField = $item.find('.js-compare__field--destination');
                var $resetBtn = $item.find('.js-compare__reinit');
                var $getPriceBtn = $item.find('.js-compare__main-cta');
                var $validBtn = $item.find('.js-compare__reset-cta');
                var resort = $item.data('resort');
                var $details = $block.find('.js-compare__details[data-resort="'+resort+'"]');
                var $crossedPrice = $details.find('.js-compare__crossed');
                var $finalPrice = $details.find('.js-compare__total');
                var $babyAdd = $details.find('.js-compare__babyclub--add');
                var $babyRemove = $details.find('.js-compare__babyclub--remove');
                var $tabsBlock = $details.find('.js-tabs');
                var $tabs = $tabsBlock.find('.js-tablist__link');
                var $panels = $tabsBlock.find('.js-tabcontent');
                var $btnTransport = $item.find('.js-compare__field--transport');
                var $layerTransport = $('#'+$btnTransport.attr('aria-controls'));
                var $selectTransport = $layerTransport.find('.c-select');
                var hasBaby = false;
                var transport = 190;
                var activeOldPrice;
                var activePrice;
                var options = {
                    data: infos,
                    getValue: 'name',
                    list: {
                        match: {
                            enabled: true
                        },
                        onClickEvent: function() {
                            $item.trigger('change');
                        }
                    },
                    template: {
                        type: "custom",
                        method: function(value, item) {
                            return '<span class="c-field__rank c-field__rank--' + (item.rank).toLowerCase() + '"><span>' + value + '</span></span>';
                        }
                    },
                };

                $destField.easyAutocomplete(options);

                $resetBtn.click(function(e) {
                    e.preventDefault();
                    $item.addClass('reset');
                    $details.addClass('reset');
                });

                $item.on('change', function() {
                    $details.addClass('reset');
                    $getPriceBtn.removeAttr('disabled');
                });

                $validBtn.click(function() {
                    var $resetBlock = $item.find('.js-compare__reset-fields');
                    var $checkedInput = $resetBlock.find('input:checked');
                    var inputId = $checkedInput.attr('id');
                    var newDestination = '';
                    var newDeparture = '';
                    var newTransport = '';
                    var newTravelers = '';
                    var reset = false;
                    var transport = false;

                    if (inputId.indexOf('-new') != -1) {
                        var $calendar = $('#'+$item.find('.js-compare__field--departure').attr('aria-controls')).find('.js-calendar');
                        var $travelers = $('#'+$item.find('.js-compare__field--travelers').attr('aria-controls'));
                        var $datepicker = $calendar.datepicker().data('datepicker');
                        newDestination = '';
                        newDeparture = 'Départ';
                        newTransport = 'Sans transport';
                        newTravelers = '1 adulte(s) - 0 enfant(s)';
                        
                        $datepicker.clear();
                        while (!$travelers.find('.js-updateinfo__adults .js-updateinfo__less').attr('disabled')) {
                            $travelers.find('.js-updateinfo__adults .js-updateinfo__less').trigger('click');
                        }
                        while (!$travelers.find('.js-updateinfo__children .js-updateinfo__less').attr('disabled')) {
                            $travelers.find('.js-updateinfo__children .js-updateinfo__less').trigger('click');
                        }
                        $selectTransport.val(0).trigger('change');
                        $item.find('.js-compare__field--transport .c-btn__picto--transport').removeClass('on');
                        $item.find('.js-compare__field--transport .c-btn__picto--no-transport').addClass('on');
                    } else {
                        if (inputId.indexOf('-same-a') != -1) {
                            var $target = $block.find('.js-compare__layer[data-resort="a"]');
                        } else if (inputId.indexOf('-same-b') != -1) {
                            var $target = $block.find('.js-compare__layer[data-resort="b"]');
                        } else if (inputId.indexOf('-same-c') != -1) {
                            var $target = $block.find('.js-compare__layer[data-resort="c"]');
                        }

                        if (!$target.hasClass('reset')) {
                            if ($target.find('.js-compare__field--transport .c-btn__picto--transport').hasClass('on')) {
                                $selectTransport.val(1).trigger('change');
                                $item.find('.js-compare__field--transport .c-btn__picto--transport').addClass('on');
                                $item.find('.js-compare__field--transport .c-btn__picto--no-transport').removeClass('on');
                                $details.removeClass('no-transport');
                            } else {
                                $selectTransport.val(0).trigger('change');
                                $item.find('.js-compare__field--transport .c-btn__picto--transport').removeClass('on');
                                $item.find('.js-compare__field--transport .c-btn__picto--no-transport').addClass('on');
                                $details.addClass('no-transport');
                            }

                            newDestination = $target.find('.js-compare__field--destination').val();
                            newDeparture = $target.find('.js-compare__field--departure .c-btn__text').html();
                            newTransport = $target.find('.js-compare__field--transport .c-btn__text').html();
                            newTravelers = $target.find('.js-compare__field--travelers .c-btn__text').html();
                        } else {
                            reset = true;
                        }
                    }

                    if (!reset) {
                        $item.find('.js-compare__field--destination').val(newDestination);
                        $item.find('.js-compare__field--departure .c-btn__text').html(newDeparture);
                        $item.find('.js-compare__field--transport .c-btn__text').html(newTransport);
                        $item.find('.js-compare__field--travelers .c-btn__text').html(newTravelers);
                        $item.removeClass('reset').find('input[id*="-new"]').trigger('click');
                    }
                });

                $getPriceBtn.click(function() {
                    var desti = $destField.val();

                    infos.forEach(function(e, i) {
                        if (desti == e.name) {
                            var newInfos = e;
                            var nbTabs = newInfos.tabs.length;
                            var firstOldPrice = parseInt(newInfos.tabs[0].oldprice);
                            var firstNewPrice = parseInt(newInfos.tabs[0].newprice);
                            var oldPrice = firstOldPrice + transport;
                            var newPrice = firstNewPrice + transport;

                            $tabs.removeAttr('aria-disabled');

                            if (nbTabs == 1) {
                                $tabs.eq(1).attr('aria-disabled', true);
                                $tabs.eq(2).attr('aria-disabled', true);
                            }

                            if (nbTabs == 2) {
                                $tabs.eq(2).attr('aria-disabled', true);
                            }

                            newInfos.tabs.forEach(function(tabE, tabI) {
                                $tabs.eq(tabI).data('oldprice', tabE.oldprice).data('price', tabE.newprice).find('.c-block__tabs_text--big').html(tabE.name);
                                $panels.eq(tabI).find('img').attr('src', 'img/' + tabE.img);
                            });

                            $tabs.eq(0).trigger('click');
                            $details.find('.js-compare__img').attr('src', 'img/' + newInfos.img);
                            $details.find('.js-compare__title').attr('src', 'img/' + newInfos.title);
                            $details.find('.js-compare__flight').attr('src', 'img/' + newInfos.flight);
                            firstOldPrice == 0 ? $crossedPrice.html('') : $crossedPrice.html(oldPrice).trigger('change');
                            $finalPrice.html(newPrice).trigger('change');

                            $details.removeClass('reset');
                            $getPriceBtn.attr('disabled', '');
                            $babyAdd.addClass('on');
                            $babyRemove.removeClass('on');
                            hasBaby = false;
                        }
                    });
                });

                $babyAdd.click(function() {
                    var newPrice = activePrice + transport + 310;
                    var newCrossedPrice = activeOldPrice + transport + 310;

                    $finalPrice.html(newPrice).trigger('change');
                    activeOldPrice == 0 ? $crossedPrice.html('') : $crossedPrice.html(newCrossedPrice).trigger('change');
                    $babyAdd.removeClass('on');
                    $babyRemove.addClass('on');
                    hasBaby = true;
                });

                $babyRemove.click(function() {
                    var newPrice = activePrice + transport;
                    var newCrossedPrice = activeOldPrice + transport;

                    $finalPrice.html(newPrice).trigger('change');
                    activeOldPrice == 0 ? $crossedPrice.html('') : $crossedPrice.html(newCrossedPrice).trigger('change');
                    $babyAdd.addClass('on');
                    $babyRemove.removeClass('on');
                    hasBaby = false;
                });

                $tabsBlock.on('tabchange', function() {
                    var $tabs = $tabsBlock.find('.js-tablist__item');
                    var $tab1 = $tabs.eq(0).find('.js-tablist__link');
                    var $tab2 = $tabs.eq(1).find('.js-tablist__link');
                    var $tab3 = $tabs.eq(2).find('.js-tablist__link');
                    var tab1oldprice = $tab1.data('oldprice');
                    var tab2oldprice = $tab2.data('oldprice');
                    var tab3oldprice = $tab3.data('oldprice');
                    var tab1price = $tab1.data('price');
                    var tab2price = $tab2.data('price');
                    var tab3price = $tab3.data('price');
                    var $activeTab = $tabsBlock.find('.js-tablist__link[aria-selected="true"]');
                    var activeIndex = $activeTab.parent('.js-tablist__item').index();
                    activeOldPrice = parseInt($activeTab.data('oldprice'));
                    activePrice = parseInt($activeTab.data('price'));
                    var newCrossedPrice = activeOldPrice + transport;
                    var newPrice = activePrice + transport;

                    $activeTab.find('.c-block__tabs_text--price').html('Inclus');

                    if (activeIndex == 0) {
                        if (!$tab2.attr('aria-disabled')) {
                            $tab2.find('.c-block__tabs_text--price').html(tab2price-activePrice).trigger('change');
                            $tab2.find('.c-block__tabs_text--price').prepend('+');
                        }

                        if (!$tab3.attr('aria-disabled')) {
                            $tab3.find('.c-block__tabs_text--price').html(tab3price-activePrice).trigger('change');
                            $tab3.find('.c-block__tabs_text--price').prepend('+');
                        }
                    } else if (activeIndex == 1) {
                        if (!$tab1.attr('aria-disabled')) {
                            $tab1.find('.c-block__tabs_text--price').html(-(activePrice-tab1price)).trigger('change');
                        }

                        if (!$tab3.attr('aria-disabled')) {
                            $tab3.find('.c-block__tabs_text--price').html(tab3price-activePrice).trigger('change');
                            $tab3.find('.c-block__tabs_text--price').prepend('+');
                        }
                    } else if (activeIndex == 2) {
                        if (!$tab1.attr('aria-disabled')) {
                            $tab1.find('.c-block__tabs_text--price').html(-(activePrice-tab1price)).trigger('change');
                        }

                        if (!$tab2.attr('aria-disabled')) {
                            $tab2.find('.c-block__tabs_text--price').html(-(activePrice-tab2price)).trigger('change');
                        }
                    }

                    if (hasBaby) {
                        if (activeOldPrice > 0) newCrossedPrice = activeOldPrice + 310;
                        newPrice = activePrice + 310;
                    }

                    activeOldPrice == 0 ? $crossedPrice.html('') : $crossedPrice.html(newCrossedPrice).trigger('change');
                    $finalPrice.html(newPrice).trigger('change');
                });

                $selectTransport.on('change', function() {
                    if ($selectTransport.val() == '0') {
                        transport = 0;
                        $details.addClass('no-transport');
                    } else {
                        transport = 190;
                        $details.removeClass('no-transport');
                    }
                });

                $getPriceBtn.trigger('click');
            });
        });
        return $(this);
    };
})(jQuery);