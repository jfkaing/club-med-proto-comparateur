(function($) {
    /* --- SLIDER MODULE --- */

    $.fn.slider = function() {
        var $wrap = $(this);

        $.each($wrap, function(i, element) {
            var $block = $(element);
            var $list = $block;
            var options = {};

            $block.find('.js-slider__list').length ? $list = $block.find('.js-slider__list') : $list = $block;
            options.variableWidth = $block.hasClass('js-slider--variable') ? true : false;
            options.arrows = $block.hasClass('js-slider--noarrow') ? false : true;
            options.infinite = $block.hasClass('js-slider--noloop') ? false : true;
            if ($block.find('.js-slider__prev').length) options.prevArrow = $block.find('.js-slider__prev');
            if ($block.find('.js-slider__next').length) options.nextArrow = $block.find('.js-slider__next');
            if ($block.attr('data-slides')) options.slidesToShow = $block.data('slides');
            if ($block.hasClass('js-slider--nomobile')) options.responsive = [{ breakpoint: 992, settings: 'unslick' }];
            $list.slick(options);
        });
        return $(this);
    };
})(jQuery);