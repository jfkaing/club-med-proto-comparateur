(function($) {
    /* --- ADDCOMPARE MODULE --- */

    $.fn.addcompare = function() {
        var $wrap = $(this);

        $.each($wrap, function(i, element) {
            var $block = $(element);
            var $switch = $block.find('.js-addcompare__switch');
            var $calendar = $('#'+$switch.attr('aria-controls'));
            var $calendarItem = $calendar.find('.js-calendar');
            var $btn = $block.find('.js-addcompare__btn');
            var $btnNb = $btn.find('.js-addcompare__btn_nb');
            var $items = $block.find('.js-addcompare__item > a');
            var isChecked = false;
            var nbAdded = 0;

            // click on 'add & compare' button
            $switch.on('change', function() {
                // if switch is on
                if ($switch.is(':checked')) {
                    // uncheck to show calendar first
                    $switch.prop('checked', false);
                    // trigger if user clicks on a day
                    $calendarItem.on('clickdate', function() {
                        if (!isChecked && $calendar.hasClass('c-layer--addcompare')) {
                            $switch.prop('checked', true);
                            $block.removeClass('calendar-on').addClass('on');
                            isChecked = true;
                            $calendar.slideUp().removeClass('on');
                        }
                    });
                    // hide calendar
                    if ($calendar.hasClass('on')) {
                        $calendar.removeClass('on');
                        $block.removeClass('calendar-on');
                        $switch.removeClass('js-layer--on').addClass('js-layer--off');
                    // show calendar
                    } else {
                        if ($('.js-layer--on').length) $('.js-layer--on').trigger('click');
                        $calendar.addClass('on');
                        $block.addClass('calendar-on');
                        $switch.removeClass('js-layer--off').addClass('js-layer--on');
                    }
                // if switch is off
                } else {
                    isChecked = false;
                    nbAdded = 0;
                    $btnNb.html(nbAdded);
                    $block.removeClass('on');
                    $items.removeClass('added');
                    $btn.removeClass('on').attr('disabled', '');
                }
            });

            // click on item
            $items.on('click', function(e) {
                var $tItem = $(this);

                if (isChecked) {
                    e.preventDefault();

                    // remove an item
                    if ($tItem.hasClass('added') && nbAdded > 0) {
                        $tItem.removeClass('added');
                        nbAdded--;
                        $btnNb.html(nbAdded);
                        if (nbAdded == 0) $btn.removeClass('on').attr('disabled', '');
                    // add an item
                    } else {
                        if (nbAdded < 3) {
                            $tItem.addClass('added');
                            nbAdded++;
                            $btnNb.html(nbAdded);
                            if (!$btn.hasClass('on')) $btn.addClass('on').removeAttr('disabled');
                        }
                    }
                }
            });
        });
        return $(this);
    };
})(jQuery);