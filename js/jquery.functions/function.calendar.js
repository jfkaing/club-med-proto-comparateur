(function($) {
    /* --- CALENDAR MODULE --- */

    $.fn.calendar = function() {
        var $wrap = $(this);

        $.each($wrap, function(i, element) {
            var $calendarItem = $(element);
            var today = new Date();
            var tomorrow = new Date();

            // init datepicker
            tomorrow.setDate(today.getDate()+1);
            var $datepicker = $calendarItem.datepicker({
                language: 'fr',
                navTitles: {
                    days: 'MM yyyy'
                },
                minDate: tomorrow
            });

            $calendarItem.find('.datepicker--cell.-selected-').click(function(e) {
                e.preventDefault();
            });
        });
        return $(this);
    };
})(jQuery);