/*----------- cookie: Object Cookie setter, getter & remover ----------*/

var cookie = (function (doc) {
    var set = function (name, value, expires, path, domain, secure) {
        /*
         * @param {String} name
         * @param {String|Number|Object} value
         * @param {Number|Date} expire (number of days)
         * @param {String} path
         * @param {String} domain
         * @param {boolean} secure
         *
         */
        var today = new Date();
        today.setTime(today.getTime());
        if (expires) {
            expires = expires * (1000 * 86400); //24h * 1000->milliseconds format: expire * tomorrow from now
        }
        var expires_date = new Date(today.getTime() + (expires));
        doc.cookie = name + '=' + escape(value) +
            ((expires) ? ';expires=' + expires_date.toGMTString() : '') + //expires.toGMTString()
            ((path) ? ';path=' + path : '') +
            ((domain) ? ';domain=' + domain : '') +
            ((secure) ? ';secure' : '');
    };
    var get = function (name) {
        /*
         * @param {String} name
         * @return cookie
         *
         */
        var start = doc.cookie.indexOf(name + "=");
        var len = start + name.length + 1;
        if ((!start) && (name != doc.cookie.substring(0, name.length))) {
            return null;
        }
        if (start == -1) return null;
        var end = doc.cookie.indexOf(';', len);
        if (end == -1) end = doc.cookie.length;
        return unescape(doc.cookie.substring(len, end));
    };
    var remove = function (name, path, domain) {
        /*
         * @param {String} name
         * @param {String} path
         * @param {String} domain
         *
         */
        if (this.get(name)) {
            doc.cookie = name + "=" + ((path) ? ";path=" + path : "") + ((domain) ? ';domain=' + domain : '') + ';expires=Thu, 01-Jan-1970 00:00:01 GMT';
        }
    };
    return {
        set: set,
        get: get,
        remove: remove
    }
})(document);