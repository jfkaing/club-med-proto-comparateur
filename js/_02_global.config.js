var config = (function ($, doc, win, session) {
    return {

        // styling params
        className: {
            hidden: 'u-hidden'
        },
        // front params
        cookie: {
            expiration: 365
        },
        navigator: function () {
            return checkStorage(function () {
                return JSON.parse(session.getItem('navigator'));
            })
        },
        countryCode: function () {
            return checkStorage(function () {
                return JSON.parse(session.getItem('navigator')).lng[config.navigator().ua == 'Firefox' ? 1 : 0].split("-")[1];
            });
        },
        detectIE: function() {
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf('MSIE ');
            var trident = ua.indexOf('Trident/');
            var edge = ua.indexOf('Edge/');

            if (msie > 0) {
                // IE 10 or older => return version number
                var ieV = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
                doc.querySelector('body').className += ' ie ie' + ieV;
                return true;
            } else if (trident > 0) {
                // IE 11 => return version number
                var rv = ua.indexOf('rv:');
                var ieV = parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
                doc.querySelector('body').className += ' ie ie' + ieV;
                return true;
            } else if (edge > 0) {
                // IE 12 (aka Edge) => return version number
                var ieV = parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
                doc.querySelector('body').className += ' ie ie' + ieV;
                return true;
            } else {
                // other browser
                return false;
            }
        },
        isMobile: function() {
            if ($(window).width() < 992) {
                return true;
            } else {
                return false;
            }
        }

    }
})(jQuery, document, window, window.sessionStorage);
window.config = config;