/*===========================================*/
/*					EVENTS
/*===========================================*/

(function($, doc, config) {

    $(doc).ready(function() {
        // IE detector
        config.detectIE();

        // main modules references
        $(".js-currency").currency();
        $(".js-updateinfo").updateinfo();
        $(".js-slider").slider();
        $(".js-calendar").calendar();
        $(".js-layer").layer();
        $(".js-addcompare").addcompare();
        $(".js-compare").compare();
        $(".js-comparetabs").comparetabs();

    });

})(jQuery, document, config);