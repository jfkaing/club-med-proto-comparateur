// requires
var gulp = require('gulp');

// include plugins
var plugins = require('gulp-load-plugins')();
var run = require('run-sequence');
var concat = require('gulp-concat');
var gulpif = require('gulp-if');
var util = require('gulp-util');
var injectHtml = require('gulp-inject-stringified-html');
var inject = require('gulp-inject');
var rimraf = require('rimraf');
var prettify = require('gulp-html-prettify');
var removeHtmlComments = require('gulp-remove-html-comments');
var svgInject = require('gulp-svg-inject');
var svgMin = require('gulp-svgmin');
var uglify = require('gulp-uglify');
var gutil = require('gutil');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

// paths root
var destination = './dist/';

// paths files
var main_scss = 'scss/main.scss';
var scss = 'scss/**/*.scss';
var js = 'js/**/*.js';
var images = 'img/*.+(png|jpg|jpeg|gif|svg|ico)';
var fonts = 'fonts/**/*.ttf';
var html = '**/*.html';

// paths libs
var lib = 'lib/';

// paths libs css
var libCss = [
    lib + 'slick-carousel/slick/slick.css',
    lib + 'air-datepicker/dist/css/datepicker.css',
    lib + 'flyout/jquery.flyout.css',
    lib + 'easy-autocomplete/easy-autocomplete.css',
    lib + 'easy-autocomplete/easy-autocomplete.themes.css'
];

// paths libs js
var libJsMain = [
    lib + 'jquery/dist/jquery.min.js',
    lib + 'slick-carousel/slick/slick.min.js',
    lib + 'air-datepicker/dist/js/datepicker.min.js',
    lib + 'air-datepicker/dist/js/i18n/datepicker.fr.js',
    lib + 'flyout/jquery.flyout.js',
    lib + 'jquery-accessible-tabs/jquery-accessible-tabs.js',
    lib + 'easy-autocomplete/jquery.easy-autocomplete.min.js',
    lib + 'autoNumeric/dist/autoNumeric.min.js',
    lib + 'scrollTo/jquery.scrollTo.js'
];

/* task "clean"
 ========================================================================== */

gulp.task('clean', function (callback) {
    return rimraf(destination, callback);
});

/* task "styles"
 ========================================================================== */

var cssfiles = libCss.concat(main_scss);

gulp.task('styles', function () {
    return gulp.src(cssfiles)
        .pipe(plugins.sass({
            errLogToConsole: true,
            outputStyle: 'expanded'
        })
        .on('error', plugins.sass.logError))
        .pipe(plugins.autoprefixer({
            browsers: ['> 1%', 'last 2 versions', 'Firefox ESR'],
            cascade: false
        }))
        .pipe(plugins.cssbeautify({
            indent: '  '
        }))
        .pipe(concat('style.css'))
        .pipe(gulpif(util.env.production, plugins.csso()))
        .pipe(plugins.sourcemaps.write('./'))
        .pipe(gulp.dest(destination))
        .pipe(browserSync.stream());
});

/* task "scripts"
 ========================================================================== */

var jsfiles = libJsMain.concat(js);

gulp.task('scripts', function () {
    return gulp.src(jsfiles)
        .pipe(concat('script.js'))
        .pipe(gulpif(util.env.production, plugins.uglify({
            output: {max_line_len: 1000000}
        })))
        .on('error', function (err) { gutil.log(err.toString()); })
        .pipe(plugins.sourcemaps.write('./'))
        .pipe(gulp.dest(destination));
});

/* task "images" / "woff" / "woff2" / "inject"
 ========================================================================== */

// task "images" = imagemin (source -> destination)
gulp.task('images', function () {
    return gulp.src(images)
        .pipe(plugins.cache(plugins.imagemin({
            progressive: true,
            interlaced: true
        })))
        .pipe(gulp.dest(destination + '/img/'))
});

// task "fonts" = ttf2woff (source -> destination)
gulp.task('woff', function () {
    return gulp.src(fonts)
        .pipe(plugins.ttf2woff())
        .pipe(gulp.dest(destination + '/fonts/'))
});

// task "woff2" = ttf2woff2 (source -> destination)
gulp.task('woff2', function () {
    return gulp.src(fonts)
        .pipe(plugins.ttf2woff2())
        .pipe(gulp.dest(destination + '/fonts/'))
});

// task "inject" (source -> destination)
gulp.task('inject', function () {
    return gulp.src('./*.html')
    .pipe(inject(gulp.src(['./partials/head.html']), {
        starttag: '<!-- inject:head -->',
        transform: function (filePath, file) {
            return file.contents.toString('utf8')
        }
    }))
    .pipe(inject(gulp.src(['./partials/header.html']), {
        starttag: '<!-- inject:header -->',
        transform: function (filePath, file) {
            return file.contents.toString('utf8')
        }
    }))
    .pipe(inject(gulp.src(['./partials/scripts.html']), {
        starttag: '<!-- inject:scripts -->',
        transform: function (filePath, file) {
            return file.contents.toString('utf8')
        }
    }))
    .pipe(inject(gulp.src(['./partials/footer.html']), {
        starttag: '<!-- inject:footer -->',
        transform: function (filePath, file) {
            return file.contents.toString('utf8')
        }
    }))
    .pipe(inject(gulp.src(['./partials/layer-calendar.html']), {
        starttag: '<!-- inject:layer-calendar -->',
        transform: function (filePath, file) {
            return file.contents.toString('utf8')
        }
    }))
    .pipe(inject(gulp.src(['./partials/layer-travelers.html']), {
        starttag: '<!-- inject:layer-travelers -->',
        transform: function (filePath, file) {
            return file.contents.toString('utf8')
        }
    }))
    .pipe(inject(gulp.src(['./partials/layer-transport.html']), {
        starttag: '<!-- inject:layer-transport -->',
        transform: function (filePath, file) {
            return file.contents.toString('utf8')
        }
    }))
    .pipe(inject(gulp.src(['./partials/layer-compare-calendar.html']), {
        starttag: '<!-- inject:layer-compare-calendar -->',
        transform: function (filePath, file) {
            return file.contents.toString('utf8')
        }
    }))
    .pipe(inject(gulp.src(['./partials/layer-envies.html']), {
        starttag: '<!-- inject:layer-envies -->',
        transform: function (filePath, file) {
            return file.contents.toString('utf8')
        }
    }))
    .pipe(inject(gulp.src(['./partials/layer-etat-esprit.html']), {
        starttag: '<!-- inject:layer-etat-esprit -->',
        transform: function (filePath, file) {
            return file.contents.toString('utf8')
        }
    }))
    .pipe(inject(gulp.src(['./partials/layer-voyager-avec.html']), {
        starttag: '<!-- inject:layer-voyager-avec -->',
        transform: function (filePath, file) {
            return file.contents.toString('utf8')
        }
    }))
    .pipe(svgInject())
    .pipe(removeHtmlComments())
    .pipe(prettify({
        indent_char: ' ',
        indent_size: 4
    }))
    .pipe(gulp.dest(destination));
});

/* task "build" = "glyphs" + ["css" + "js" + "images" + "symbols" + "fonts" + "inject"]
 ========================================================================== */

gulp.task('build', function (callback) {
    run('images', ['woff', 'woff2', 'scripts', 'styles', 'inject'], callback)
});

/* task "dist" = "clean" + "build"
 ========================================================================== */

gulp.task('dist', function (callback) {
    run('clean', ['build'], callback)
});

/* task "watch"
 ========================================================================== */

gulp.task('js-watch', ['scripts'], function (done) {
    browserSync.reload();
    done();
});

gulp.task('html-watch', ['inject'], function (done) {
    browserSync.reload();
    done();
});

gulp.task('watch', ['browser-sync'], function () {
    gulp.watch(scss, ['styles']);
    gulp.watch(js, ['js-watch']);
    gulp.watch(html, ['html-watch']);
    gulp.watch(images, ['images']);
});

/* task "default" = "build"
 ========================================================================== */

gulp.task('default', ['dist']);

/* task "browser-sync"
 ========================================================================== */

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: destination
        }
    });
});